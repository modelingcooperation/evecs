;;; This file is part of Evecs. It is subject to the license terms in the
;;; the LICENSE.txt file found in the top-level directory of this distribution
;;; and at https://gitlab.com/x-mod/evecs/-/blob/master/LICENSE.txt. No part of
;;; Evecs, including this file, may be copied, modified, propagated, or
;;; distributed except according to the terms contained in the LICENSE file.

(ns team.xmod.evecs-test
  "Tests for Evecs."
  (:require [clojure.test :refer [deftest is]]
            [team.xmod.evecs :as ev]))

(def counter-world
  "World with only one object: the counter."
  {::counter {::count 0}})

(defn counting-sys
  "Increase the counter."
  [world]
  (update-in world [::counter ::count] inc))

;;; Send one tick event and check that counter system gets activated and
;;; increases the counter.
(deftest counter
  (let [final-state (ev/event-loop [[counting-sys ::ev/tick]]
                                   [::ev/tick]
                                   counter-world)]
    (is (= 1 (get-in final-state [::counter ::count])))))

;;; Check that infinite iterations stop at ev/MAX_ITERS
(deftest max-iters
  (let [final-state (ev/event-loop [[counting-sys ::count]]
                                   [::count]
                                   counter-world)]
    (is (= ev/MAX_ITERS (get-in final-state [::counter ::count])))))

(defn multicounting-sys
  "Increase the counter if it's < 5."
  [world]
  (let [c (get-in world [::counter ::count])]
    (if (< c 5)
      (assoc-in world [::counter ::count] (inc c))
      world)))

;;; Send one tick even to a counter that gets retriggered by it's own update
;;; until the value is 5 and check that it goes up to 5.
(deftest multi-counter
  (let [final-state (ev/event-loop [[multicounting-sys ::ev/tick ::count]]
                                   [::ev/tick]
                                   counter-world)]
    (is (= 5 (get-in final-state [::counter ::count])))))

(def ping-pong-world
  {::counter {::count 0}
   ::player1 {::ball true}
   ::air {::flying-to nil}
   ::player2 {::ball nil}
   ::air-log {::records []}})

(defn ping-pong-sys
  "Throw the ball between players if the counter is < 10."
  [world]
  (if (< (get-in world [::counter ::count]) 10)
    (cond
      (get-in world [::player1 ::ball])
      (-> world
          (assoc-in [::player1 ::ball] nil)
          (assoc-in [::air ::flying-to] ::player2))
      (get-in world [::player2 ::ball])
      (-> world
          (assoc-in [::player2 ::ball] nil)
          (assoc-in [::air ::flying-to] ::player1))
      :else ; none of the players have the ball -- it's flying.
      (let [target (get-in world [::air ::flying-to])]
        (-> world
            (assoc-in [::air ::flying-to] nil)
            (assoc-in [target ::ball] true))))
    world))

(defn logging-sys
  "Log the state of the air."
  [world]
  (let [flying-to (get-in world [::air ::flying-to])
        record (if (nil? flying-to) "-" (name flying-to))]
    (update-in world [::air-log ::records] #(conj % record))))

;;; Throw a ball between player1 and player2 through the air (see the definition
;;; of `ping-pong-world` above). Counter counts the times balls is thrown or
;;; caught and the air-log keeps track of what's in the air each time it changes.
(deftest ping-pong
  (let [subscriptions [[counting-sys ::ball]
                       [logging-sys ::flying-to]
                       [ping-pong-sys ::ball]]
        final-state (ev/event-loop subscriptions
                                   [::ball]
                                   ping-pong-world)]
    (is (= 10 (get-in final-state [::counter ::count])))
    (is (= ["player2" "-" "player1" "-" "player2" "-" "player1" "-" "player2"]
           (get-in final-state [::air-log ::records])))
    (is (not (get-in final-state [::player2 ::ball])))
    (is (not (get-in final-state [::player1 ::ball])))
    (is (= ::player2 (get-in final-state [::air ::flying-to])))))

;;; Run the same ping-pong game but run each system only once per step.
(deftest stepwise-ping-pong
  (let [subscriptions [[counting-sys ::ball]
                       [logging-sys ::flying-to]
                       [ping-pong-sys ::ev/tick]]
        states (take 12 (ev/run subscriptions
                                [::ev/tick]
                                ping-pong-world))
        final-state (last states)
        penultimate-state (last (take 11 states))]
    (is (= 10 (get-in final-state [::counter ::count])))
    (is (= ["player2" "-" "player1" "-" "player2" "-"
            "player1" "-" "player2" "-"]
           (get-in final-state [::air-log ::records])))
    (is (get-in final-state [::player2 ::ball]))
    (is (not (get-in final-state [::player1 ::ball])))
    (is (not (get-in final-state [::air ::flying-to])))
    (is (= final-state penultimate-state))))

;;; Increment counter once per step until it's 10.
(deftest stepwise-counter
  (let [states (take 15
                     (ev/run-up-to [[counting-sys ::ev/tick]]
                                   [::ev/tick]
                                   #(= (get-in % [::counter ::count]) 10)
                                   counter-world))
        last-state (last states)]
    (is (= 11 (count states))) ; initial state + 10 steps with counter bumps.
    (is (= 10 (get-in last-state [::counter ::count])))))
