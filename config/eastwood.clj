(disable-warning
 {:linter :unused-ret-vals
  :for-macro 'clojure.test.check.properties/for-all
  :if-inside-macroexpansion-of #{'clojure.test.check.clojure-test/defspec}
  :reason "Return values are checked after macro expansion."})
