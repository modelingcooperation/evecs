# Evecs

Evecs (Event-based ECS) is a simple entity component system where system
activations are triggered by events (changes in the world). A system can
subscribe to some keywords and it will be triggered when a value for this key
changes in one of the entities in the world.

Run one step of the simulation:

```clojure
(require ['team.xmod.evecs :as 'ev])

; Define systems in the symulation and events that trigger them.
(def subscriptions
  [[system1 ::keyword1 ::ns/keyword2]
   [system2 ::keyword3]
   [system3 ::keyword1 ::ns/keyword4]]

(def world
  {::entity1 {::keyword1 0
              ::ns/keyword2 0}
   ::entity2 {::keyword3 0
              ::ns/keyword4 0}})

(ev/event-loop subscriptions [::keyword1] world)
```

Run simulation forever:

```clojure
; ::keyword1 will be added to the event queue at the start of each step.
(ev/run subscriptions [::keyword1] world)
```

Run until a predicate is true, returning states of the world up to and
including the first where it's true:

```clojure
(ev/run-up-to subscriptions [::keyword1] done? world)
````