;;; This file is part of Evecs. It is subject to the license terms in the
;;; the LICENSE.txt file found in the top-level directory of this distribution
;;; and at https://gitlab.com/x-mod/evecs/-/blob/master/LICENSE.txt. No part of
;;; Evecs, including this file, may be copied, modified, propagated, or
;;; distributed except according to the terms contained in the LICENSE file.

(ns team.xmod.evecs
  "Event-based ECS.")

(def MAX_ITERS
  "Maximal number of iterations in an event loop."
  42)

(defn diff-keys
  "List the keys that have different values between two maps."
  [a b]
  (if (= a b)
    () ; shortcut for identical maps.
    (let [a-keys (keys a)
          b-keys (keys b)]
      (filter #(not= (% a) (% b))
              (distinct (concat a-keys b-keys))))))

(defn detect-changed-keys
  "Detect the entity keys that changed between two worlds."
  [a b]
  (set (mapcat #(diff-keys (% a) (% b))
               (diff-keys a b))))

(defn- get-subscribers
  "Return subscriber systems based on subscriptons and changed keys."
  [subscriptions changed-keys]
  (let [changed? #(contains? changed-keys %)]
    (map first
         (filter #(some changed? (rest %))
                 subscriptions))))

(defn event-loop
  "Run event loop until we run out of events or up to `MAX_ITERS`.

  - `subscriptions` is a vector of vectors: [system keyword keyword ...]
    (a system is a function: world -> world and keywords are subscriptions
     to changes in the world, in other words, events)
  - `events` is a sequence of initial events that would trigger some systems.
  - `world` is the state of the world to start from
    (a world is a map from entity-id to entity, where each entity is a map)."
  [subscriptions init-events world]
  (loop [i 1
         world world
         changed-keys (set init-events)]
    (let [systems-to-call (get-subscribers subscriptions changed-keys)
          updated-world (reduce #(%2 %1) world systems-to-call)]
      (if (or (>= i MAX_ITERS) (= world updated-world))
        updated-world
        (recur (inc i)
               updated-world
               (detect-changed-keys world updated-world))))))

(defn run
  "Run event loop repeatedly and return a sequence of world states.

  Events in `events` will be injected in the world at the beginning of each
  iteration. See `event-loop` for more detail on parameters."
  [subscriptions step-events world]
  (iterate (partial event-loop subscriptions step-events) world))

(defn take-up-to
  "Return elements from coll up to the first one for which pred is true."
  [pred coll]
  (lazy-seq
   (when-let [s (seq coll)]
     (if (pred (first s))
       (list (first s))
       (cons (first s) (take-up-to pred (rest s)))))))

(defn run-up-to
  "Like `run` but will stop after `pred` is true on a world state."
  [subscriptions init-events pred world]
  (take-up-to pred (run subscriptions init-events world)))

